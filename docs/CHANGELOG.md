# Changelog

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/compare/release/1.0.0...release/1.1.0) (2024-10-08)


### Features

* add CI files ([b5770f8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/b5770f85adb329fa8424f307efac240930db33b1))
* deploy dev image ([c207222](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/c2072226d17fdc19335ae06d15dab820c11843fc))


### Bug Fixes

* deploy appVersion to 1.11.0 ([3a6aba8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/3a6aba8e1a48929ad1623f44e7b9a7676295ed4b))

## 1.0.0 (2024-06-13)


### Features

* add CI files ([0c07fa6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/0c07fa6eb330a67a5e4fb77152a12c92c26ec6c5))
* deploy dev image ([53daf3d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/53daf3d4d68f93575383500d015eefa72958f380))
* first testing release of tubes chart ([556ef21](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/556ef21f46e1cb7cafa0527f27ae01291f44b35b))


### Bug Fixes

* always pull image ([f764545](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/f76454516ab89949b3c51897a8ffa6bf92f5cc74))
* **CI:** activate commit lint ([f9821a1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/f9821a198e4d5f898cf213e076c3c01a0cda93ee))
* **CI:** activate commit lint ([2ebe0f9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/2ebe0f9fb5ec64d97fb57238fff68ab830d3a781))
* **CI:** bypass bad commit message ([4e121d6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/4e121d6f5520d95370016e648484bfce2d6840ba))
* deploy appversion 1.10.0 ([c4be684](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/c4be684d849ae9594e35472a5cc1676bfd52f230))
* deploy testing appVersion ([f6ab4be](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/f6ab4be1fec39d5517ec8167947811f4589db7e0))
* modify values.yaml for preprod ([38105f6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/38105f68b17be36f3b19d486e527d70b48e49b37))
* new testing helm ([73919fd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/73919fd95779dbdbf32e9e37c16ae60947b2c633))
* set ingress controller to nginx ([e6b0fc8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/e6b0fc872b4be44f93cd688d44c7a37e06ebc369))
* use ingress controller secret ([d3906c7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/tubes/tubes-helm-chart/commit/d3906c758ac4f6f93890ab0db6ab9c8bb716ba63))
